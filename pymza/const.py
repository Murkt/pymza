FETCH_BUFFER_SIZE_BYTES = 1024 * 1024  # 1Mb
MAX_FETCH_BUFFER_SIZE_BYTES = FETCH_BUFFER_SIZE_BYTES * 8
SOCKET_TIMEOUT = 120
GET_MESSAGE_TIMEOUT = SOCKET_TIMEOUT - 10  # must be less than SOCKET_TIMEOUT